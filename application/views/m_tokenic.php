<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Konfirmasi Data</div>

    <div class="panel-body">
    <!-- id_gelombang -->
    <!-- id_ruangan -->
      <input type="hidden" name="id_ujian" id="id_ujian" value="<?php echo $du['id']; ?>">
      <input type="hidden" name="_tgl_mulai" id="_tgl_mulai" value="<?php echo $tgl_mulai; ?>">
      <input type="hidden" name="_terlambat" id="_terlambat" value="<?php echo $terlambat; ?>">
      <div class="col-md-7">
        <div class="panel panel-default">
          <div class="panel-body">
            <table class="table table-bordered">
              <tr><td width="35%">Nama</td><td width="65%"><?php echo $dp['nama_peserta']; ?></td></tr>
              <tr><td>Kode</td><td><?php echo $dp['kode']; ?></td></tr>
              <tr><td>Jml Soal</td><td><?php echo $du['js']; ?></td></tr>
              <tr><td>Waktu</td><td><?php echo $du['wkt']; ?> menit</td></tr>
            </table>
          </div>
        </div>
      </div>
      
      <div class="col-md-5">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="alert alert-info">
              Klik Tombol Mulai Untuk Mengerjakan Soal Kembali
            </div>

            <a href="#" class="btn btn-success btn-lg" id="tbl_mulai" onclick="return konfirmasi_token(<?php echo $du['id_sesi']; ?>)"><i class="fa fa-check-circle"></i> MULAI</a>
            
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


</div>
