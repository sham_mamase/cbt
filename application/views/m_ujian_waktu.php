<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Data Waktu Ujian
      <div class="tombol-kanan">
        <a class="btn btn-success btn-sm tombol-kanan" href="#" onclick="return m_waktu_ujian_e(0);"><i class="glyphicon glyphicon-plus"></i> &nbsp;&nbsp;Tambah</a>        
       <!-- <a class="btn btn-warning btn-sm tombol-kanan" href="<?php echo base_url(); ?>upload/format_import_siswa.xlsx" ><i class="glyphicon glyphicon-download"></i> &nbsp;&nbsp;Download Format Import</a> -->
      <!--  <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/m_siswa/import" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import</a> -->
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="15%">Jenis Soal</th>
            <th width="10%">Ruangan</th>
            <th width="10%">Sesi</th>
            <th width="25%">Tgl / Jam Mulai</th>
            <th width="10%">Terlambat</th>
            <th width="25%">Aksi</th>
          </tr>
        </thead>

        <tbody></tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>
                    
<div class="modal fade" id="m_waktu_ujian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Atur Waktu Ujian</h4>
      </div>
      <div class="modal-body">
          <form name="f_waktu_ujian" id="f_waktu_ujian" onsubmit="return m_waktu_ujian_s();">
            <input type="hidden" name="id" id="id" value="0">
              <table class="table table-form">
                <tr>
                <td style="width: 25%">Jenis Soal</td>
                <td style="width: 75%">
                  <select name="id_sesi" id="id_sesi" class="form-control">
                    <option value="">--Pilih Jenis Soal--</option>
                    <?php 
                        foreach ($dt_tes as $dt) {
                    ?>
                        <option value="<?php echo $dt->id_jenis_soal ?>"><?php echo $dt->jenis_soal ?></option>
                    <?php
                        }
                     ?>
                  </select>
                </td>
                </tr>
                <tr>
                <td style="width: 25%">Ruangan</td>
                <td style="width: 75%">
                  <select name="id_ruangan" id="id_ruangan" class="form-control">
                    <option value="">--Pilih Ruangan--</option>
                    <?php 
                        foreach ($ruangan as $rg) {
                    ?>
                        <option value="<?php echo $rg->id_ruangan ?>"><?php echo $rg->ruangan ?></option>
                    <?php
                        }
                     ?>
                  </select>
                </td>
                </tr>
                <tr>
                  <td>Sesi</td>
                  <td>
                  <select name="id_gelombang" id="id_gelombang" class="form-control">
                    <option value="">--Pilih Sesi--</option>
                    <?php 
                        foreach ($gel as $g) {
                    ?>
                        <option value="<?php echo $g->id_gelombang ?>">Sesi <?php echo $g->gelombang ?></option>
                    <?php
                        }
                     ?>
                  </select>
                  </td>
                </tr>
                <tr>
                <td style="width: 25%">Tgl Mulai/Jam</td>
                <td>
                  <input type="date" name='tgl_mulai' class="form-control" style="width: 150px; display: inline; float: left" id="tgl_mulai" placeholder="contoh(2017-06-12) thn-bln-tgl" required>
                  <input type="time" name='wkt_mulai' class="form-control" style="width: 100px; display: inline; float: left" id="wkt_mulai" placeholder="contoh(08:00)" required>
                </td>
                </tr>
                <tr><td style="width: 25%">Terlambat</td><td style="width: 75%"><input type="text" class="form-control" name="terlambat" id="terlambat" required placeholder="Menit"></td></tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-minus-circle"></i> Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>
