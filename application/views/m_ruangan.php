<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Data Ruangan Ujian
      <div class="tombol-kanan">
        <a class="btn btn-success btn-sm tombol-kanan" href="#" onclick="return m_ruangan_e(0);"><i class="glyphicon glyphicon-plus"></i> &nbsp;&nbsp;Tambah</a>
      </div>
    </div>
    <div class="panel-body">


      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="10%">ID</th>
            <th width="20%">Nama Ruangan</th>
            <th width="15%">Jml Peserta</th>
            <th width="15%">Token Ujian</th>
            <th width="35%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>
                    




<div class="modal fade" id="m_ruangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Data Ruangan Ujian</h4>
      </div>
      <div class="modal-body">
          <form name="f_ruangan" id="f_ruangan" onsubmit="return m_ruangan_s();">
            <input type="hidden" name="id" id="id" value="0">
              <table class="table table-form">
                <tr><td style="width: 25%">ID Ruangan</td><td style="width: 75%"><input type="text" class="form-control" name="id_ruangan" id="id_ruangan" required></td></tr>
                <tr><td style="width: 25%">Nama Ruangan</td><td style="width: 75%"><input type="text" class="form-control" name="ruangan" id="ruangan" required></td></tr>
                <tr><td style="width: 25%">Token Ujian</td><td style="width: 75%"><input type="text" class="form-control" name="token_ujian" id="token_ujian" required></td></tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary">Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>