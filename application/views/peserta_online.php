<div class="row col-md-12">
  <div class="panel panel-info">
    <div class="panel-heading">Peserta Yang Sedang Online Ruangan <?php  echo $rg_aktif ?></div>
    <div class="panel-body">


      <table class="table table-bordered">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="10%">Kode</th>
            <th width="25%">Nama</th>
          </tr>
        </thead>

        <tbody>
          <?php 
            if (!empty($peserta_online)) {
              $no = 1;
              foreach ($peserta_online as $po) {
          ?>
              <tr>
                  <td class="ctr"><?php echo $no ?></td>
                  <td><?php echo $po->kode ?></td>
                  <td><?php echo $po->nama ?></td>
              </tr>
          <?php  
            $no++;
              } 
            } else {
          ?>
            <tr><td colspan="5">Belum ada Peserta Yang Online</td></tr>
          <?php 
            }
           ?>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>                  