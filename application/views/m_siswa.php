<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Data Siswa
      <div class="tombol-kanan">
      <a class="btn btn-success btn-sm tombol-kanan" href="#" onclick="return m_siswa_e(0);"><i class="glyphicon glyphicon-plus"></i> &nbsp;&nbsp;Tambah</a>        
      <a class="btn btn-warning btn-sm tombol-kanan" href="<?php echo base_url(); ?>upload/format_import_siswa.xlsx" ><i class="glyphicon glyphicon-download"></i> &nbsp;&nbsp;Download Format Import</a>
      <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/m_siswa/import" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import</a>
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="15%">Kode</th>
            <th width="25%">Nama</th>
            <th width="20%">Ruangan</th>
            <th width="10%">Kelompok</th>
            <th width="25%">Aksi</th>
          </tr>
        </thead>

        <tbody></tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>
                    
<div class="modal fade" id="m_siswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Data Siswa</h4>
      </div>
      <div class="modal-body">
          <form name="f_siswa" id="f_siswa" onsubmit="return m_siswa_s();">
            <input type="hidden" name="id" id="id" value="0">
              <table class="table table-form">
                <tr><td style="width: 25%">Kode Peserta</td><td style="width: 75%"><input type="text" class="form-control" name="kode" id="kode" required></td></tr>
                <tr><td style="width: 25%">Nama</td><td style="width: 75%"><input type="text" class="form-control" name="nama_peserta" id="nama_peserta" required></td></tr>
                
                <tr>
                <td style="width: 25%">Ruangan</td>
                <td style="width: 75%">
                  <select name="id_ruangan" id="id_ruangan" class="form-control">
                    <?php 
                        foreach ($ruangan as $rg) {
                    ?>
                        <option value="<?php echo $rg->id_ruangan ?>"><?php echo $rg->ruangan ?></option>
                    <?php
                        }
                     ?>
                  </select>
                </td>
                </tr>
                <tr>
                <td style="width: 25%">Kelompok</td>
                <td style="width: 75%">
                  <select name="id_pilihan_peserta" id="id_pilihan_peserta" class="form-control">
                    <?php 
                        foreach ($pilihan_peserta as $pil) {
                    ?>
                        <option value="<?php echo $pil->id_pilihan_peserta ?>"><?php echo $pil->pilihan_peserta ?></option>
                    <?php
                        }
                     ?>
                  </select>
                </td>
                </tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-minus-circle"></i> Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>
