<div class="row col-md-12">
  <div class="panel panel-info">
    <div class="panel-heading">Data Peserta </div>
    <div class="panel-body">


      <table class="table table-bordered">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="10%">Kode</th>
            <th width="25%">Nama</th>
            <th width="20%">Aksi</th>
          </tr>
        </thead>

        <tbody>
          <?php 
            if (!empty($detail_ruangan)) {
              $no = 1;
              foreach ($detail_ruangan as $d) {
          ?>
              <tr>
                  <td class="ctr"><?php echo $no ?></td>
                  <td><?php echo $d->kode ?></td>
                  <td><?php echo $d->nama_peserta ?></td>
                  <td><button type="button" data-toggle="modal" data-target="#myModal<?php echo $d->kode ?> " class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-th-list" style="margin-left: 0px; color: #fff"></i>&nbsp;&nbsp;Atur Sesi</button></td>
              </tr>
          <?php  
            $no++;
              } 
            } else {
          ?>
            <tr><td colspan="5">Belum ada data</td></tr>
          <?php 
            }
           ?>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>
                    


<?php 
    foreach ($detail_ruangan as $gel) {
?>
<div class="modal fade" id="myModal<?php echo $gel->kode ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Edit Sesi <?php echo $gel->nama_peserta ?></h4>
      </div>
      <div class="modal-body">
          <?php echo form_open('adm/edit_gelombang'); ?>
            <input type="hidden" name="id_gel" value="<?php echo $gel->kode ?>">
            <?php 
            foreach($gelombang as $g) {
            if ($g->id_gelombang == $gel->id_gelombang) {
                $status = "checked";
              } else {
                $status = "";
              }

            ?>
              <label for="">Sesi <?php echo $g->gelombang ?></label> &nbsp; <input type="radio" name="gel" value="<?php echo $g->id_gelombang ?>" <?php echo $status ?>> &nbsp;
            <?php 
              } ?> 
            <!--<label for="">Gel I</label> &nbsp; <input type="radio"  name="gel" checked> &nbsp;-->              
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary">Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
      </div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>
<?php 
    }
 ?>

