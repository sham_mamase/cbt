<div class="row col-md-12">
  <div class="panel panel-info">
    <div class="panel-heading"> <?php  echo $soal ?></div>
    <div class="panel-body">


      <table class="table table-bordered">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="10%">Jawaban</th>
            <th width="25%">Status</th>
            <th width="25%">Aksi</th>
          </tr>
        </thead>

        <tbody>
          <?php 
            if (!empty($opsi)) {
              $no = 1;
              foreach ($opsi as $o) {
          ?>
              <tr>
                  <td class="ctr"><?php echo $no ?></td>
                  <td><?php echo $o->jawaban ?><p><img src="<?php echo base_url(); ?>upload/gambar_opsi/<?php echo $o->file ?>" alt=""></p></td>
                  <td><?php echo $o->benar ?></td>
                  <td>
                    <div class="btn-group">
                            <a href="'.base_url().'adm/m_soal/edit/'.$d['id'].'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Edit</a>
                            <a href="'.base_url().'adm/m_soal/hapus/'.$d['id'].'" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Hapus</a>
                          </div>
                  </td>
              </tr>
          <?php  
            $no++;
              } 
            } else {
          ?>
            <tr><td colspan="5">Belum ada opsi jawaban</td></tr>
          <?php 
            }
           ?>
        </tbody>
      </table>
    
      </div>
    </div>
  </div>
</div>                  