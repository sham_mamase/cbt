<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_adm extends CI_Model {

	

	public function __construct()
	{
		parent::__construct();	
	}

	function get_peserta()
	{
		$query = $this->db->query("SELECT a.id, a.kode, a.nama_peserta, c.ruangan FROM t_peserta a, t_ruangan c WHERE a.id_ruangan = c.id_ruangan");
		// cek data apakah sudah ada
		if ($query->num_rows() > 0) {
			return $query->result();
		}

	}

}
// tes tambah
/* End of file M_adm.php */
/* Location: ./application/models/M_adm.php */